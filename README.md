Это - исходный код бота для Mastodon, который ходит по лесу и постит процедурно сгенерированные описания.

Код адаптирован из старого черновика невыпущенной игры.

Написан на CoffeeScript, движок процедурной генерации — [Improv](https://github.com/sequitur/improv)

Текст (всё, что в папке `data`) открыт под лицензией [CC-BY-NC 4.0.](https://creativecommons.org/licenses/by-nc/4.0/deed.ru)

Код открыт под лицензией MIT, см. LICENSE.txt
